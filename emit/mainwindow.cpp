#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(on_pushButton_clicked()));
    connect(this,SIGNAL(mySignal()),SLOT(mySlot()));
    connect(this,SIGNAL(mySignal(int)),SLOT(mySlot(int)));
    connect(this,SIGNAL(mySignalParam(int,int)),SLOT(mySlotParam(int,int)));
    emit mySignal();
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::mySlot()
{
    QMessageBox::about(this,"Tsignal", "hello this is demo");
}
void MainWindow::mySlot(int x)
{
    char s[256];
    sprintf(s,"x:%d",x);
    QMessageBox::about(this,"Tsignal", s);
}
void MainWindow::mySlotParam(int x,int y)
{
    char s[256];
    sprintf(s,"x:%d y:%d",x,y);
    QMessageBox::about(this,"Tsignal", s);
}
void MainWindow::on_pushButton_clicked()
{
    emit mySignal(2);
}
