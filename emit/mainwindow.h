#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void mySignal();
    void mySignal(int x);
    void mySignalParam(int x,int y);

private slots:
    void on_pushButton_clicked();
    void mySlot();
    void mySlot(int x);
    void mySlotParam(int x,int y);


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
