#include "qclickedlabel.h"

QClickedLabel::QClickedLabel(QWidget *parent) :
    QLabel(parent)
{
}

void QClickedLabel::mouseReleaseEvent(QMouseEvent * ev)
{
    Q_UNUSED(ev)
    emit clicked();
}
