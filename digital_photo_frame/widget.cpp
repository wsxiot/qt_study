#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    display();
}

void Widget::GetImgs(QString dir)
{
    QDirIterator it(dir,QDir::Files|QDir::Dirs|QDir::NoDotAndDotDot);
    while (it.hasNext())
    {
        QString name = it.next();
        QFileInfo info(name);
        if (info.isDir())
        {
            this->GetImgs(name);
        }
        else
        {
            if (info.suffix() == "jpg"
                    || info.suffix() == "jpeg"
                    || info.suffix() == "bmp"
                    || info.suffix() == "png"
                    || info.suffix() == "gif")
            {
                qDebug()<<name;
                flist.append(name);
            }
        }
    }
}

void Widget::changeimg()
{
    if(count == flist.size())
        count=0;
    QString fpname = flist.at(count);
    pix.load(fpname);
    pix = pix.scaled(this->width(),this->height(),Qt::KeepAspectRatio);
    if(fpname.endsWith("gif"))
    {
        movie.setFileName(fpname);
        movie.setScaledSize(QSize(pix.width(),pix.height()));
        ui->imglabel->setMovie(&movie);
        movie.start();
    }
    else
    {
        movie.stop();
        ui->imglabel->setPixmap(pix);
    }
    count++;
}

void Widget::display()
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    QRect screenRect = QApplication::desktop()->availableGeometry();
    this->resize(screenRect.width(),screenRect.height());
    ui->imglabel->resize(this->width(),this->height());
    ui->imglabel->setAlignment(Qt::AlignCenter);

    count=0;
    timerstatus=false;
    flist.clear();
    GetImgs("./images");

    if(flist.size() == 0)
    {
        ui->imglabel->setText("u盘没插或者u盘没图片");
    }
    else
    {
        changeimg();
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()),this,SLOT(changeimg()));
        timer->start(1500);
        timerstatus = true;
    }
    connect(ui->imglabel, SIGNAL(clicked()), this, SLOT(ImgLable_cliecked()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::ImgLable_cliecked()
{
    if(timerstatus){
         timer->stop();
         timerstatus = false;
    }else{
        timer->start(1500);
        timerstatus = true;
    }
}
