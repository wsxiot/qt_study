#-------------------------------------------------
#
# Project created by QtCreator 2017-05-23T14:34:55
#
#-------------------------------------------------

QT       += core gui

TARGET = digital_photo_frame
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    qclickedlabel.cpp

HEADERS  += widget.h \
    qclickedlabel.h

FORMS    += widget.ui
