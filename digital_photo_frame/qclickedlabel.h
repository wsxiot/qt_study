#ifndef QCLICKEDLABEL_H
#define QCLICKEDLABEL_H

#include <QLabel>
#include <QWidget>
#include <QMouseEvent>

class QClickedLabel : public QLabel
{
    Q_OBJECT
public:
    explicit QClickedLabel(QWidget *parent=0);

protected:
    virtual void mouseReleaseEvent(QMouseEvent * ev);
    
signals:
    void clicked(void);

public slots:
    
};

#endif // QCLICKEDLABEL_H
