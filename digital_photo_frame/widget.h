#ifndef WIDGET_H
#define WIDGET_H

#include <QObject>
#include <QWidget>
#include <QDebug>
#include <QSet>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QStringList>
#include <QList>
#include <QDirIterator>
#include <QPixmap>
#include <QTimer>
#include <QDesktopWidget>
#include <QRect>
#include <QMessageBox>
#include <QMovie>
#include <QImage>
#include <QSize>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void changeimg();
    void ImgLable_cliecked();
    
private:
    Ui::Widget *ui;
    QStringList flist;
    void GetImgs(QString dir);
    QTimer *timer;
    QPixmap pix;
    int count;
    void display();
    QMovie movie;
    bool timerstatus;
};

#endif // WIDGET_H
